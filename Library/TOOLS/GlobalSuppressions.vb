Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"

' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Data.LLBLGen.EntityExtensions")> 

#End Region

#Region "CA1704:IdentifiersShouldBeSpelledCorrectly"

<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBaseSub`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisherSub`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher3`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase1`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher2Sub`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher`1", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase2Sub`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase3`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher1Sub`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase1Sub`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher2`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher1`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase2`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase`1", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntitySyncMultiPublisherBase3`4", MessageId:="Multi")> 

#End Region


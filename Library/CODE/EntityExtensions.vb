﻿Imports System.Runtime.CompilerServices
Imports SD.LLBLGen.Pro.ORMSupportClasses
Imports System.Collections.Generic
Imports System.Windows.Forms

''' <summary>
''' Includes extensions for LLBLGEN Entities.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/19/2010" by="David Hary" revision="1.2.3975.x">
''' Created
''' </history>
Public Module EntityExtensions

#Region " GRID "

    ''' <summary>
    ''' Hides all columns.
    ''' </summary>
    ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub HideColumns(ByVal grid As DataGridView)
        If grid IsNot Nothing Then
            For Each column As DataGridViewColumn In grid.Columns
                column.Visible = False
            Next
        End If
    End Sub

    ''' <summary>
    ''' Hides the columns by the <paramref name="columnNames">column names</paramref>.
    ''' </summary>
    ''' <param name="grid">The grid.</param>
    ''' <param name="columnNames">The ciolumn names.</param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub HideColumns(ByVal grid As DataGridView, ByVal columnNames As String())
        If grid IsNot Nothing Then
            For Each column As DataGridViewColumn In grid.Columns
                If Not columnNames.Contains(column.Name) Then
                    column.Visible = False
                End If
            Next
        End If
    End Sub

#End Region

#Region " ADAPTER "

    ''' <summary>
    ''' Returns the list of field names
    ''' </summary>
    ''' <param name="entity">Reference to an entity implementing the <see cref="IEntity">entity interface</see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function GetFieldNames(ByVal entity As IEntity2) As String()
        Dim l As New List(Of String)
        If entity IsNot Nothing Then
            For Each field As IEntityField2 In entity.Fields
                l.Add(field.Name)
            Next
        End If
        Return l.ToArray
    End Function

    ''' <summary>
    ''' Returns the list of primary key field names
    ''' </summary>
    ''' <param name="entity">Reference to an entity implementing the <see cref="IEntity">entity interface</see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function GetPrimaryKeyFieldNames(ByVal entity As IEntity2) As String()
        Dim l As New List(Of String)
        If entity IsNot Nothing Then
            For Each field As IEntityField2 In entity.PrimaryKeyFields
                l.Add(field.Name)
            Next
        End If
        Return l.ToArray
    End Function

    ''' <summary>
    ''' Hides columns that are not defined as entity fields.
    ''' </summary>
    ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
    ''' <param name="entity">Specifies the entity displayed in the datagrid.</param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub HideColumnsByEntityFields(ByVal grid As DataGridView, ByVal entity As IEntity2)
        If grid IsNot Nothing Then
            grid.HideColumns(entity.GetFieldNames)
        End If
    End Sub

#End Region

#Region " SELF SERVICING "

    ''' <summary>
    ''' Returns the list of field names
    ''' </summary>
    ''' <param name="entity">Reference to an entity implementing the <see cref="IEntity">entity interface</see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function GetFieldNames(ByVal entity As IEntity) As String()
        Dim l As New List(Of String)
        If entity IsNot Nothing Then
            For Each field As IEntityField In entity.Fields
                l.Add(field.Name)
            Next
        End If
        Return l.ToArray
    End Function

    ''' <summary>
    ''' Returns the list of primary key field names
    ''' </summary>
    ''' <param name="entity">Reference to an entity implementing the <see cref="IEntity">entity interface</see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function GetPrimaryKeyFieldNames(ByVal entity As IEntity) As String()
        Dim l As New List(Of String)
        If entity IsNot Nothing Then
            For Each field As IEntityField In entity.PrimaryKeyFields
                l.Add(field.Name)
            Next
        End If
        Return l.ToArray
    End Function


    ''' <summary>
    ''' Hides columns that are not defined as entity fields.
    ''' </summary>
    ''' <param name="grid">The <see cref="DataGridView">grid</see></param>
    ''' <param name="entity">Specifies the entity displayed in the datagrid.</param>
    ''' <remarks></remarks>
    <Extension()> _
    Public Sub HideColumnsByEntityFields(ByVal grid As DataGridView, ByVal entity As IEntity)
        If grid IsNot Nothing Then
            grid.HideColumns(entity.GetFieldNames)
        End If
    End Sub

#End Region

End Module
